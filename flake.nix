{
	description = "dyngarden";

	inputs = {
		nixpkgs.url      = "github:nixos/nixpkgs/nixos-unstable";
		flake-parts.url  = "github:hercules-ci/flake-parts";
		crane.url        = "github:ipetkov/crane";
		rust-overlay.url = "github:oxalica/rust-overlay";

		rust-overlay.inputs.nixpkgs.follows = "nixpkgs";
	};

	outputs = inputs@{ flake-parts, ... }: flake-parts.lib.mkFlake { inherit inputs; } {
		perSystem = { self', system, pkgs, ... }: {
			_module.args.pkgs = import inputs.nixpkgs {
				inherit system;
				overlays = [ (import inputs.rust-overlay) ];
			};
		} // (let
			craneLib = (inputs.crane.mkLib pkgs).overrideToolchain (p:
				p.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
					extensions = [ "rust-src" "rustfmt" "rust-analyzer" "clippy" ];
				})
			);

			commonArgs = {
				src = craneLib.cleanCargoSource ./.;
				strictDeps = true;
			};

			dyngarden = craneLib.buildPackage (commonArgs // {
				cargoArtifacts = craneLib.buildDepsOnly commonArgs;
			});
		in {
			devShells.default = craneLib.devShell {
				# inherit inputs from checks.
				checks = self'.checks;

				packages = with pkgs; [
					cargo-expand
					# rust-analyzer

					pkg-config
				];
			};

			checks = {
				inherit dyngarden;
			};

			packages = {
				dyngarden = pkgs.symlinkJoin {
					name = "dyngarden";
					paths = [ dyngarden ];
					buildInputs = with pkgs; [ makeWrapper ];
					postBuild = ''
						wrapProgram $out/bin/dyngarden \
							--set _APP_STATIC_ASSETS "${./assets}"
					'';

				};
				default = self'.packages.dyngarden;
			};

			apps = {
				dyngarden = { type = "app"; program = "${self'.packages.dyngarden}/bin/dyngarden"; };
				default = self'.apps.dyngarden;
			};
		});
		systems = [
			"x86_64-linux"
			"aarch64-linux"
		];
	};
}
