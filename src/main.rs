#![feature(iter_array_chunks)]

use {
	color_eyre::Result,
	colored::Colorize,
	image::{GenericImage, GenericImageView, ImageReader, Rgba, RgbaImage},
	palette::{blend::Compose, Srgba},
	rand::{distributions::Uniform, seq::SliceRandom, thread_rng, Rng},
	std::{
		env,
		path::{Path, PathBuf},
	},
};

fn main() -> Result<()> {
	let asset_path = env::var("_APP_STATIC_ASSETS")
		.expect("failed to find asset dir")
		.parse::<PathBuf>()
		.expect("failed to parse asset dir path");

	let ground_image = ImageReader::open(asset_path.join("ground.png"))?.decode()?;

	let mut img = RgbaImage::new(128, 24);
	let img_height = img.height();

	let flowers = load_flowers(&asset_path)?;

	const FLOWER_MARGIN: u32 = 4;

	// place a few flowers
	let mut i = FLOWER_MARGIN;
	loop {
		let flower = flowers.choose(&mut thread_rng()).unwrap();

		// advance randomly
		i += thread_rng().sample(Uniform::new(
			// some arbitrary spacing limits
			(flower.width() as f32 / 2.0) as u32,
			(flower.width() as f32 * 1.5) as u32,
		));

		// don't try placing flowers after their designated area
		if i >= img.width() - FLOWER_MARGIN {
			break;
		}

		overlay(
			&mut img,
			flower,
			i - flower.width() / 2,
			img_height - flower.height() - 4,
		);
	}

	overlay(
		&mut img,
		&ground_image,
		0,
		img_height - ground_image.height(),
	);

	print!("{}", image_to_string(&img));

	Ok(())
}

fn load_flowers(asset_path: &Path) -> Result<Vec<image::DynamicImage>, color_eyre::Report> {
	(1..=100)
		.map(|i| -> Result<_> {
			Ok(ImageReader::open(asset_path.join(format!("flowers/{:03}.png", i)))?.decode()?)
		})
		.collect::<Result<Vec<_>>>()
}

fn overlay(
	img: &mut impl GenericImage<Pixel = Rgba<u8>>,
	stamp: &impl GenericImageView<Pixel = Rgba<u8>>,
	x: u32,
	y: u32,
) {
	// if the stamp would end up outside of the original image, don't draw it
	if img.width() - x < stamp.width() || img.height() - y < stamp.height() {
		return;
	}

	for (stamp_x, stamp_y, stamp_pixel) in stamp.pixels() {
		let (target_x, target_y) = (stamp_x + x, stamp_y + y);

		let original_pixel = Srgba::from_components(img.get_pixel(target_x, target_y).0.into())
			.into_format::<_, f32>();
		let stamp_pixel = Srgba::from_components(stamp_pixel.0.into()).into_format::<_, f32>();

		img.put_pixel(
			target_x,
			target_y,
			Rgba(
				stamp_pixel
					.over(original_pixel)
					.into_format::<_, u8>()
					.into_components()
					.into(),
			),
		)
	}
}

fn image_to_string(img: &impl GenericImageView<Pixel = Rgba<u8>>) -> String {
	let mut row_iter = (0..img.height()).array_chunks::<2>();

	let out_rows = img.height().div_ceil(2);
	let mut out = String::with_capacity(
		(img.width()
			* out_rows
			* (
				// colour seq (approximate)
				30
				// half block char
				+ 3
				// reset colour seq
				+ 4
			)
			// newlines
			+ out_rows) as usize,
	);

	// print all pairs of rows
	for [row_upper, row_lower] in &mut row_iter {
		for col in 0..img.width() {
			let pixel_upper = img.get_pixel(col, row_upper);
			let pixel_lower = img.get_pixel(col, row_lower);

			out.push_str(
				&"▀"
					.truecolor(pixel_upper.0[0], pixel_upper.0[1], pixel_upper.0[2])
					.on_truecolor(pixel_lower.0[0], pixel_lower.0[1], pixel_lower.0[2])
					.to_string(),
			);
		}
		out.push('\n');
	}
	// in case the row count was odd, print the remaining one
	if let Some(row) = row_iter.into_remainder().and_then(|mut iter| iter.next()) {
		for col in 0..img.width() {
			let pixel = img.get_pixel(col, row);

			out.push_str(
				&"▀"
					.truecolor(pixel.0[0], pixel.0[1], pixel.0[2])
					.to_string(),
			);
		}
		out.push('\n');
	}

	out
}
